'use strict';

var SmallApp = require('./SmallApp.jsx');
var LoginPage = require('./LoginPage.jsx');
var React = require('react');
var Router = require('react-router');
var Route = Router.Route;
var DefaultRoute = Router.DefaultRoute;


var content = document.getElementById('content');

var Routes = (

  <Route name="app" path="/" handler={SmallApp}>
    <DefaultRoute name="default-login" handler={LoginPage} />
    <Route name="login" path="/login" handler={LoginPage} />
  </Route>

);

Router.run(Routes, function (Handler) {
  React.render(<Handler/>, content);
});
