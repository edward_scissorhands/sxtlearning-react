'use strict';

var ServerActionCreators = require('../actions/ServerActionCreators');
var request = require('superagent');


var WebAPIUtils = {
  login: function(email, password) {
    request.get('http://localhost:4000/api/web/login')
      .query('login=' + email)
      .query('password=' + password)
      .query('client_platform=iOS')
      .query('client_version=1.0')
      .end(function(err, res){
        if(res) {
          if(res.error) {
            var errors = JSON.parse(res.text);
            ServerActionCreators.receiveLogin(null, errors);
          } else {
            var json = JSON.parse(res.text);
            ServerActionCreators.receiveLogin(json, null);
          }
        }
      });
  },

  wechat: function(){
    request.get('http://localhost:4000/api/web/wechat_authorizations/new')
      .end(function(err, res) {
        if(res) {
          if(res.error) {
            //errors
            console.log(res.error)
          } else {
            var json = JSON.parse(res.text);
            console.log(json)
          }
        }
      })
  }
};

module.exports = WebAPIUtils;
