'use strict';

var React = require('react/addons');
var ReactTransitionGroup = React.addons.TransitionGroup;
var RouteHandler = require('react-router').RouteHandler;
var SessionStore = require('../stores/SessionStore');

// CSS
require('normalize.css');
require('../styles/main.css');

//var imageURL = require('../images/yeoman.png');

function getStateFromStores() {
  return {
    isLoggedIn: SessionStore.isLoggedIn(),
    email: SessionStore.getEmail()
  };
}

var SmallApp = React.createClass({

  getInitialState: function() {
    return getStateFromStores();
  },

  componentDidMount: function() {
    SessionStore.addChangeListener(this._onChange);
  },

  componentWillUnmount: function() {
    SessionStore.removeChangeListener(this._onChange);
  },

  _onChange: function() {
    this.setState(getStateFromStores());
  },

  render: function() {
    return (
      <div className="main">
        <ReactTransitionGroup transitionName="fade">
          <RouteHandler />
        </ReactTransitionGroup>
      </div>
    );
  }
});

module.exports = SmallApp;
