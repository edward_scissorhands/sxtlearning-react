'use strict';

var SmallAppDispatcher = require('../dispatcher/SmallAppDispatcher');
var SmallConstants = require('../constants/SmallConstants');

var ActionTypes = SmallConstants.ActionTypes;

var ServerActionCreators = {
  receiveLogin: function(json, errors) {
    SmallAppDispatcher.handleServerAction({
      type: ActionTypes.LOGIN_RESPONSE,
      json: json,
      errors: errors
    });
  }
};

module.exports = ServerActionCreators;