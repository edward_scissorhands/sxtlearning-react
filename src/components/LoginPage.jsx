'use strict';

var React = require('react/addons');
var SessionActionCreators = require('../actions/SessionActionCreators');
var SessionStore = require('../stores/SessionStore');
var Router = require('react-router');
var _email = SessionStore.getEmail();

var LoginPage = React.createClass({

  mixins: [ Router.Navigation ],

  getInitialState: function() {
    return {
      errors: [],
      isLoggedIn: SessionStore.isLoggedIn()
    };
  },

  componentDidMount: function() {
    SessionStore.addChangeListener(this._onChange);
  },

  componentWillUnmount: function() {
    SessionStore.removeChangeListener(this._onChange);
  },

  _onChange: function() {
    this.setState({
      logging: null,
      errors: SessionStore.getErrors(),
      email : localStorage.getItem('email'),
      isLoggedIn : localStorage.getItem('accessToken') ? true : false
    });

  },

  _onSubmit: function(e) {
    e.preventDefault();
    this.setState({
      logging: "You are logging"
    });
    var email = this.refs.email.getDOMNode().value;
    var password = this.refs.password.getDOMNode().value;
    SessionActionCreators.login(email, password);
  },

  // Clean login data, remove this after logout finish
  _cleanLogin: function() {
    localStorage.clear();
    this.setState({
      isLoggedIn: false
    })
  },

  _onWechatLogin: function(e) {
    e.preventDefault();
    this.setState({
      wechat_token: "I need wechat token"
    });
    SessionActionCreators.wechat();
  },

  render: function() {

    var current = this.state.isLoggedIn ? <p>Login in successfully, welcome {this.state.email}</p> : <p>No email</p>;

    var login_form = (
      <div>
        <form onSubmit={this._onSubmit}>
          <div>
            <label htmlFor="email">Email</label>
            <input type="text" name="email" ref="email" style={{color: "#000"}} />
            <br />
            <label htmlFor="password">Password</label>
            <input type="password" name="password" ref="password" style={{color: "#000"}} />
          </div>
          <button type="submit" style={{color: "#000"}}>Login</button>
        </form>
        <button style={{color: "#000"}} onClick={this._onWechatLogin}>Wechat Login</button>
      </div>
    );

    var clean_login_data = (
      <button onClick={this._cleanLogin} type="button" style={{color: "#000"}}>Clean Login Data</button>
    );

    var error = (
      <p style={{color: "#e65"}}>{this.state.errors}</p>
    );

    return (
      <div className="login">
        { error }
        { current }
        { this.state.isLoggedIn ? clean_login_data : login_form }
        <p>{ this.state.logging ? this.state.logging : null }</p>
        <p>{this.state.wechat_token}</p>
      </div>
    );
  }
});

module.exports = LoginPage;
