'use strict';
var SmallDispatcher = require('../dispatcher/SmallAppDispatcher');
var SmallConstants = require('../constants/SmallConstants');
var WebAPIUtils = require('../utils/WebAPIUtils');

var ActionsTypes = SmallConstants.ActionTypes;

var SessionActionCreators = {
  login: function(email, password) {
    SmallDispatcher.handleViewAction({
      type: ActionsTypes.LOGIN_REQUEST,
      email: email,
      password: password
    });
    WebAPIUtils.login(email, password);
  },
  wechat: function() {
    SmallDispatcher.handleViewAction({
      type: ActionsTypes.WECHAT_LOGIN
    });
    WebAPIUtils.wechat();
  }
};

module.exports = SessionActionCreators;
