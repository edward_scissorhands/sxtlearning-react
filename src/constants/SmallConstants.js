var keyMirror = require('keyMirror');

module.exports = {
  ActionTypes: keyMirror({
    LOGIN_REQUEST: null,
    LOGIN_RESPONSE: null,
    WECHAT_LOGIN: null
  })
};
