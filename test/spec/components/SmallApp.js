'use strict';

describe('SmallApp', () => {
  let React = require('react/addons');
  let SmallApp, component;

  beforeEach(() => {
    let container = document.createElement('div');
    container.id = 'content';
    document.body.appendChild(container);

    SmallApp = require('components/SmallApp.jsx');
    component = React.createElement(SmallApp);
  });

  it('should create a new instance of SmallApp', () => {
    expect(component).toBeDefined();
  });
});
