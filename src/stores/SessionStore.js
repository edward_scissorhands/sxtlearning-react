'use strict';

var SmallAppDispatcher = require('../dispatcher/SmallAppDispatcher');
var SmallConstant = require('../constants/SmallConstants');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var ActionTypes = SmallConstant.ActionTypes;
var CHANGE_EVNET = 'change';

var _accessToken = localStorage.getItem('accessToken');
var _email = localStorage.getItem('email');
var _errors = [];

var SessionStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {
    this.emit(CHANGE_EVNET);
  },

  addChangeListener: function(callback) {
    this.on(CHANGE_EVNET, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVNET, callback);
  },

  isLoggedIn: function() {
    return _accessToken ? true : false;
  },

  getAccessToken: function() {
    return _accessToken;
  },

  getEmail: function() {
    return _email;
  },

  getErrors: function() {
    return _errors;
  }

});

SessionStore.dispatchToken = SmallAppDispatcher.register(function(payload){
  var action = payload.action;

  switch(action.type) {
    case ActionTypes.LOGIN_RESPONSE:
      if(action.json && action.json.user['authentication_token']) {
        localStorage.setItem('accessToken', action.json.user['authentication_token']);
        localStorage.setItem('email', action.json.user['email']);
        _errors = null;
      }
      if(action.errors) {
        var errors = {
          "001": "Wrong Credentials",
          "002": "User Not Found",
          "003": "Already Submitted",
          "004": "Resource not found"
        };
        _errors = errors[action.errors['error_code']];
      }
      SessionStore.emitChange();
      break;
    default:
  }
  return true;
});

module.exports = SessionStore;
